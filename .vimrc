call pathogen#infect() 
set nocompatible
syntax on
set rnu
filetype plugin indent on
let mapleader="\<space>"
colorscheme blackboard 
" let g:mapleader='\'
set hidden
set expandtab
set sw=2
set ts=2
set incsearch
set hlsearch
set wildmenu
set wildignore=*.swp,*.bak,*.pyc,*.class,*.o
"set wildmode=list:longest,full
set bs=indent,eol,start
set encoding=utf-8
set fileencoding=utf-8
set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo
set nobackup
set showcmd
set noswapfile
set mouse=a

map <c-o> <Leader><Leader>w
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l
" cd to change to open directory.
map <leader>cd :cd %:p:h<cr>

nmap <leader>sa :cs add cscope.out<cr>
nmap <leader>ss :cs find s <C-R>=expand("<cword>")<cr><cr>
nmap <leader>sg :cs find g <C-R>=expand("<cword>")<cr><cr>
nmap <leader>sc :cs find c <C-R>=expand("<cword>")<cr><cr>

nmap <leader>tc :ts <C-R>=expand("<cword>")<cr><cr>
"nmap <leader>st :cs find t <C-R>=expand("<cword>")<cr><cr>
"nmap <leader>se :cs find e <C-R>=expand("<cword>")<cr><cr>
"nmap <leader>sf :cs find f <C-R>=expand("<cfile>")<cr><cr>
"nmap <leader>si :cs find i <C-R>=expand("<cfile>")<cr><cr>
"nmap <leader>sd :cs find d <C-R>=expand("<cword>")<cr><cr>


if has("autocmd") 
  " autocmd FileType javascript set omnifunc=jscomplete#CompleteJS
  autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
  autocmd FileType css set omnifunc=csscomplete#CompleteCSS
  autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
  autocmd Filetype java set completefunc=javacomplete#Complete
  autocmd Filetype ruby set omnifunc=rubycomplete#Complete
  autocmd FileType java nmap <F11> :!find . -iname '*.java' > cscope.files<CR>
        \:!cscope -b -q -k -i cscope.files -f cscope.out<CR>
        \:cs reset<CR>
  autocmd FileType    cpp,c nnoremap <Leader>11 :!find . -iname '*.c' -o -iname '*.cpp' -o -iname '*.h' -o -iname '*.hpp' -iname '*.s' -iname '*.S' > cscope.files<CR>
        \:!cscope -b -q -k -i cscope.files -f cscope.out<CR>
        \:cs reset<CR>
  autocmd FileType java inoremap <buffer> <c-x><c-o> <c-x><c-u>
endif 

" Complete options (disable preview scratch window)
set completeopt=longest,menuone
" Limit popup menu height
set pumheight=10
" SuperTab option for context aware completion
" Disable auto popup, use <Tab> to autocomplete
let g:NERDTreeQuitOnOpen=1
let NERDTreeIgnore=['\.pyc', '\~$', '\.swo$', '\.swp$', '\.git', '\.hg', '\.svn', '\.bzr','\.class','\.o']
nnoremap <Leader>lf :CtrlP<CR>
nnoremap <Leader>lb :CtrlPBuffer<CR>
nnoremap <Leader>lq :CtrlPTag<CR>
let g:ctrlp_working_path_mode = 0
nnoremap <Leader>ln :NERDTreeToggle<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => MISC
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm
let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
" let g:SuperTabDefaultCompletionType="context"
let g:markdown_fenced_languages = ['ruby', 'erb=eruby', 'cpp']

function! OpenURL(url)
  if has("win32")
    exe "!start cmd /cstart /b ".a:url.""
  elseif $DISPLAY !~ '^\w'
    exe "silent !sensible-browser \"".a:url."\" 2>/dev/null &"
  else
    exe "silent !sensible-browser -T \"".a:url."\" 2>/dev/null &"
  endif
  redraw!
endfunction
command! -nargs=1 OpenURL :call OpenURL(<q-args>)
" open URL under cursor in browser
nnoremap gb :OpenURL <cfile><CR>
nnoremap gA :OpenURL http://www.answers.com/<cword><CR>
nnoremap gG :OpenURL http://www.google.com/search?q=<cword><CR>
nnoremap gW :OpenURL %:p<CR> 
vmap <Leader>s :SlimuxREPLSendSelection<CR>
vmap <Leader>lt :ConqueTerm /bin/bash<CR>
let g:clang_use_library=1
let g:clang_library_path='/usr/lib/llvm-3.4/lib/'
let g:clang_complete_auto = 0
let g:clang_conceal_snippets=1
let g:clang_complete_copen=1
let g:clang_user_options='-Wc++11-extensions -std=c++11'
let g:clang_complete_hl_errors=1
let g:clang_complete_macros=1

let g:syntastic_cpp_compiler_options = ' -std=c++11'

let g:jscomplete_use = ['dom']

let g:ctrlp_extensions = ['tag']
